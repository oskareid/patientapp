package no.ntnu.idatt;
import no.ntnu.idatt.fileHandler.ImportOrExportCSV;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * This test Class consist of nested classes with methods testing
 * for both valid and invalid input regarding the
 * import and export functionality of the application.
 */

public class ImportOrExportCSV_Test {

    private final File testFile = new File("test");
    private final Patient patientOne = new Patient("Geir","Stegen", "12345678901", "Obesity", "Dr Schwartz");
    private final Patient patientTwo = new Patient("Per","Iversen", "12345678902", "Cancer", "Dr House");

    private final String DELIMITER = ";";
    private final String SEPARATOR = "\n";


    @Nested
    public class ImportTesting {

        PatientRegister testRegister = new PatientRegister();
        ImportOrExportCSV importerAndExporter = new ImportOrExportCSV();


        @Test
        public void canImportToPrePopulatedList() throws IOException {

            testRegister.addPatient(patientOne);
            testRegister.addPatient(patientTwo);

            Assertions.assertEquals(2, testRegister.getPatients().size());
            ArrayList<Patient> importedFromFile = null;
            Files.writeString(Path.of(testFile.getAbsolutePath()), "First Name;Last Name;Social Security Number;Diagnosis;General Practitioner\n" +
                    "Roar;Strand;12312312312;Anger issues;Manny\n" +
                    "Preben;Fjord;11223344556;Unknown;Sofia B\n" +
                    "Joar;Nilsen;66442386419;Covid19;Dr Wuhan");
            try{
                 importedFromFile = importerAndExporter.importRegister(testFile);
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }
            assert importedFromFile != null;
            Assertions.assertEquals(3, importedFromFile.size());
        }



        @Test
        public void canImportToEmptyList() throws IOException {
            Assertions.assertEquals(0, testRegister.getPatients().size());

            ArrayList<Patient> importedFromFile = null;
            Files.writeString(Path.of(testFile.getAbsolutePath()), "First Name;Last Name;Social Security Number;Diagnosis;General Practitioner\n" +
                    "Roar;Strand;12312312312;Anger issues;Manny\n" +
                    "Preben;Fjord;11223344556;Unknown;Sofia B\n" +
                    "Joar;Nilsen;66442386419;Covid19;Dr Wuhan");

            try{
                importedFromFile = importerAndExporter.importRegister(testFile);
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }
            assert importedFromFile != null;
            Assertions.assertEquals(3, importedFromFile.size());

        }


    }

    @Nested
    public class ExportTesting {
        PatientRegister testRegister = new PatientRegister();
        ImportOrExportCSV importerAndExporter = new ImportOrExportCSV();

        @Test
        public void canExportPopulatedListToFile() throws IOException {
            testRegister.addPatient(patientOne);
            testRegister.addPatient(patientTwo);

            String ExpectedValues = "First Name;Last Name;Social Security Number;Diagnosis;General Practitioner\n"+
                    patientOne.getFirstName()+DELIMITER+patientOne.getLastName()+DELIMITER+
                    patientOne.getSocialSecurityNumber()+DELIMITER+patientOne.getDiagnosis()+DELIMITER+
                    patientOne.getGeneralPractitioner()+SEPARATOR+
                    patientTwo.getFirstName()+DELIMITER+patientTwo.getLastName()+DELIMITER+
                    patientTwo.getSocialSecurityNumber()+DELIMITER+patientTwo.getDiagnosis()+DELIMITER+
                    patientTwo.getGeneralPractitioner()+SEPARATOR;

            importerAndExporter.exportRegister(testFile, testRegister.getPatients());
            String actualValues = Files.readString(Path.of(testFile.getAbsolutePath()));

            Assertions.assertEquals(ExpectedValues, actualValues);

        }
    }
}
