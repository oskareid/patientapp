package no.ntnu.idatt;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * This Class consist of nested classes and methods
 * responsible for testing the backend-logic of the
 * application.
 */

public class PatientRegisterTest {

    private final PatientRegister testRegister = new PatientRegister();
    private final Patient patientOne = new Patient("Roar", "Boar", "12345678901", "Cancer", "Lisa Pleasa");
    private final Patient patientTwo = new Patient("Hannah", "Montana", "10987654321", "PTSD", "Kim Larsen");
    private final Patient patientThree = new Patient("Johannes", "Johansen", "65430971628", "Anorexia", "Jamie Oliver");
    private final Patient patientWithUsedSSN = new Patient("Per", "Persen", "12345678901", "Imposter syndrome", "Needs Jesus");
    private final Patient patientWithNonDigitInSSN = new Patient("David", "Letterman", "79P34f242j1", "Criminal", "Local dealer");
    private final Patient patientWithShortSSN = new Patient("Frank", "Short", "6382749", "dyscalculia", "Personal math teacher");
    private final Patient patientWithLongSSN = new Patient("Kenneth", "Long", "8902375482642342", "inverse dyscalculia", "Frank");


    @Nested
    public class canAddPatientsToRegister {

        @Test
        public void addUniquePatients() {
            testRegister.addPatient(patientOne);
            testRegister.addPatient(patientTwo);
            testRegister.addPatient(patientThree);

            Assertions.assertEquals(3, testRegister.getPatients().size());

            for(Patient patient: testRegister.getPatients()) {
                Assertions.assertTrue(testRegister.getPatients().contains(patient));
            }
        }


    }

    @Nested
    public class failsToAddPatientWithInvalidSSN {

        @Test
        public void failToAddPatientWithUsedSSn() {
            testRegister.addPatient(patientOne);
            Assertions.assertEquals(1, testRegister.getPatients().size());

            testRegister.addPatient(patientWithUsedSSN);
            Assertions.assertEquals(1, testRegister.getPatients().size());
            Assertions.assertEquals(testRegister.getPatients().get(0), patientOne);
        }

        @Test
        public void failsToAddPatientWithOtherThanDigitSSN() {
            testRegister.addPatient(patientWithNonDigitInSSN);

            Assertions.assertFalse(testRegister.getPatients().contains(patientWithNonDigitInSSN));
        }

        @Test
        public void failsToAddPatientWithSSNLessOrGreaterThan11() {
            testRegister.addPatient(patientWithShortSSN);
            testRegister.addPatient(patientWithLongSSN);

            Assertions.assertFalse(testRegister.getPatients().contains(patientWithShortSSN));
            Assertions.assertFalse(testRegister.getPatients().contains(patientWithLongSSN));
        }
    }
}
