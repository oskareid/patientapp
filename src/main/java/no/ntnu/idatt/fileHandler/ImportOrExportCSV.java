package no.ntnu.idatt.fileHandler;

import no.ntnu.idatt.Patient;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class represents an instance of a importer/exporter of csv files.
 */

public class ImportOrExportCSV {


    // Formatting variables
    private final String COLUMN_ONE_HEAD = "First Name";
    private final String COLUMN_TWO_HEAD = "Last Name";
    private final String COLUMN_THREE_HEAD = "Social Security Number";
    private final String COLUMN_FOUR_HEAD = "Diagnosis";
    private final String COLUMN_FIVE_HEAD = "General Practitioner";
    private final String DELIMITER = ";";
    private final String PATIENT_SEPARATOR = "\n";


    /**
     *
     * @param file local file user want to import data from.
     * @return Arraylist of Patient instances made from file data.
     * @throws FileNotFoundException if file was not found.
     */

    public ArrayList<Patient> importRegister(File file) throws FileNotFoundException {
        ArrayList<Patient> patients = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            br.readLine(); // Ignores headers
            String currentRow;
            while ((currentRow = br.readLine()) != null) {

                Patient toAdd = null;
                ArrayList<String> personInfo = new ArrayList<>(Arrays.asList(currentRow.split(DELIMITER)));

                if(personInfo.size() == 5) {
                    toAdd = new Patient(personInfo.get(0), personInfo.get(1), personInfo.get(2),
                                        personInfo.get(3), personInfo.get(4));

                }
                if(toAdd != null) patients.add(toAdd);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return patients;
    }

    /**
     *
     * @param file local file user want data exported in to.
     * @param patientList list of Patient instances to get data from.
     */

    public void exportRegister(File file, List<Patient> patientList) {

        try(FileWriter fileWriter = new FileWriter(file)) {

            fileWriter.append(COLUMN_ONE_HEAD+DELIMITER+COLUMN_TWO_HEAD+DELIMITER+
                    COLUMN_THREE_HEAD+DELIMITER+COLUMN_FOUR_HEAD+DELIMITER+COLUMN_FIVE_HEAD+ PATIENT_SEPARATOR);

            for(Patient patient: patientList) {
                fileWriter.append(patient.getFirstName());
                fileWriter.append(DELIMITER);

                fileWriter.append(patient.getLastName());
                fileWriter.append(DELIMITER);

                fileWriter.append(patient.getSocialSecurityNumber());
                fileWriter.append(DELIMITER);

                fileWriter.append(patient.getDiagnosis());
                fileWriter.append(DELIMITER);

                fileWriter.append(patient.getGeneralPractitioner());
                fileWriter.append(PATIENT_SEPARATOR);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
