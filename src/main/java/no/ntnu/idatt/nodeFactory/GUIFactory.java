package no.ntnu.idatt.nodeFactory;

import javafx.scene.Node;

import java.io.IOException;

/** <Not in use>
 * Represents a Graphical User Interface factory for convenience.
 *
 * Operates as a 'middleman' between class NodeConstructor and controller classes.
 */

public class GUIFactory {

    /**
     *
     * @param nodeName type of node developer wants returned to controller
     * @return node
     * @throws IOException
     */

    public static Node getNewNode(String nodeName) throws IOException {
        NodeConstructor nodeConstructor = new NodeConstructor();
        Node toReturn;
        switch (nodeName) {
            case "menuBar":
                toReturn = nodeConstructor.constructNode("menuBar");
                break;
            case "tableView":
                toReturn = nodeConstructor.constructNode("tableView");
                break;
            default:
                return null;
        }
        return toReturn;
    }
}
