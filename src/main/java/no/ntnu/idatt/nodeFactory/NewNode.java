package no.ntnu.idatt.nodeFactory;

import javafx.scene.Node;

import java.io.IOException;

public interface NewNode {
    Node constructNode(String nodeName) throws IOException;
}
