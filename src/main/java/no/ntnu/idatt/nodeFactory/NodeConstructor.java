package no.ntnu.idatt.nodeFactory;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.Node;
import javafx.scene.control.*;
import no.ntnu.idatt.Patient;
import no.ntnu.idatt.controller.MainController;

import java.io.IOException;

/**<Not in use>
 * Class represents a constructor for different GUI elements
 * inheriting from the abstract class Node
 */

public class NodeConstructor implements NewNode {
    private MainController controller = new MainController();

    /**
     *
     * @param nodeName passed to Class GUIFactory, defines type of Node to construct
     * @return Node
     * @throws IOException
     */


    @Override
    public Node constructNode(String nodeName) throws IOException {
        if(nodeName.equals("menuBar")) {

            Menu file = new Menu("File");
            Menu edit = new Menu("Edit");
            Menu about = new Menu("About");

            SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
            MenuItem importCSV = new MenuItem("Import from .CSV");
            MenuItem exportCSV = new MenuItem("Export to .CSV");
            importCSV.setOnAction(event -> controller.importFromCSV());
            exportCSV.setOnAction(event -> controller.exportToCSV());
            file.getItems().addAll(importCSV, exportCSV, separatorMenuItem);

            MenuItem addPatient = new MenuItem("Add new patient");
            MenuItem editPatient = new MenuItem("Edit selected patient");
            MenuItem deletePatient = new MenuItem("Delete selected patient");
            addPatient.setOnAction(event -> controller.openNewPatientWindow(event));
            editPatient.setOnAction(event -> controller.openEditPatientWindow(event));
            deletePatient.setOnAction(event -> controller.deletePatient());
            edit.getItems().addAll(addPatient, editPatient, deletePatient, separatorMenuItem);

            MenuItem aboutItem = new MenuItem("About");
            aboutItem.setOnAction(event -> controller.openAboutWindow(event));
            about.getItems().add(aboutItem);

            return new MenuBar(file, edit, about);
        }

        else if(nodeName.equals("tableView")) {

            TableView<TableColumn<Patient, String>> tableView = new TableView<TableColumn<Patient, String>>();

            TableColumn<Patient, String> firstName = new TableColumn<>("First Name");
            TableColumn<Patient, String> lastName = new TableColumn<>("Last Name");
            TableColumn<Patient, String> SSN = new TableColumn<>("Social Security Number");
            TableColumn<Patient, String> diagnosis = new TableColumn<>("Diagnosis");
            TableColumn<Patient, String> generalPractitioner = new TableColumn<>("General Practitioner");

            firstName.setCellValueFactory(FS -> new ReadOnlyStringWrapper(FS.getValue().getFirstName()));
            lastName.setCellValueFactory(LS -> new ReadOnlyStringWrapper(LS.getValue().getLastName()));
            SSN.setCellValueFactory(SSNum -> new ReadOnlyStringWrapper(SSNum.getValue().getSocialSecurityNumber()));
            diagnosis.setCellValueFactory(diagnosis1 -> new ReadOnlyStringWrapper(diagnosis1.getValue().getDiagnosis()));
            generalPractitioner.setCellValueFactory(generalPrac -> new ReadOnlyStringWrapper(generalPrac.getValue().getGeneralPractitioner()));

            tableView.getItems().addAll(firstName, lastName, SSN, diagnosis, generalPractitioner);

            return tableView;
        }

        return null;
    }
}
