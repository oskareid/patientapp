package no.ntnu.idatt.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import no.ntnu.idatt.App;

/**
 * Class to represent an instance of confirmation to ensure
 * users decision to delete selected patient
 */

public class ConfirmationController {


    @FXML
    private TextField deleteField;


    @FXML
    private Label nameLabel;


    private boolean typedDelete = false;

    /**
     * Initialisation method to ensure user about
     * patient selected from tableview.
     */
    public void initialize() {
        nameLabel.setText(NewOrEditPatientController.selectedPatient.getFirstName());
        nameLabel.setTextFill(Color.RED);
        deleteField.clear();
    }


    @FXML
    void closeConfirmationWindow(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Deletes patient from system if keyword 'DELETE' is present
     * in textbox as user clicks the delete button.
     * @param event
     */
    @FXML
    void deleteSelectedPatient(ActionEvent event) {
        if(deleteField.getText().equals("DELETE")) {
            typedDelete = true;
        }
        if(typedDelete) {
            deleteField.clear();
            App.register.getPatients().remove(NewOrEditPatientController.selectedPatient);
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        }
    }

}
