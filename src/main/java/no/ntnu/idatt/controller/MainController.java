package no.ntnu.idatt.controller;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatt.App;
import no.ntnu.idatt.Patient;
import no.ntnu.idatt.sceneFactory.SceneFactory;
import no.ntnu.idatt.fileHandler.ImportOrExportCSV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Controller class to handle actions from main window of application.
 * Collaborates with all other controller classes and backend.
 */

public class MainController {

    @FXML
    private TableView<Patient> tableView;

    @FXML
    private Label statusField;

    @FXML
    private TableColumn<Patient, String> firstNameColumn;

    @FXML
    private TableColumn<Patient, String> lastNameColumn;

    @FXML
    private TableColumn<Patient, String> SSNColumn;

    @FXML
    private TableColumn<Patient, String> diagnosisColumn;

    @FXML
    private TableColumn<Patient, String> generalPractitionerCulumn;

    @FXML
    private AnchorPane topAnchor;


    private final ImportOrExportCSV middleMan = new ImportOrExportCSV();
    public static boolean adding;
    public static boolean editing;
    public Stage secondaryStage;
    private final FileChooser fileChooser = new FileChooser();





    /**
     * Method for enabling user to move the window,
     * as stage is set to undecorated.
     *
     * Read-only value for every tableview column.
     *
     */
    public void initialize() {
        topAnchor.setOnMousePressed(pressEvent -> topAnchor.setOnMouseDragged(dragEvent -> {
            App.primaryStage.setX(dragEvent.getScreenX() - pressEvent.getSceneX());
            App.primaryStage.setY(dragEvent.getScreenY() - pressEvent.getSceneY());
        }));

        firstNameColumn.setCellValueFactory(firstName -> new ReadOnlyStringWrapper(firstName.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory(lastName -> new ReadOnlyStringWrapper(lastName.getValue().getLastName()));
        SSNColumn.setCellValueFactory(SSN -> new ReadOnlyStringWrapper(SSN.getValue().getSocialSecurityNumber()));
        diagnosisColumn.setCellValueFactory(diagnosis -> new ReadOnlyStringWrapper(diagnosis.getValue().getDiagnosis()));
        generalPractitionerCulumn.setCellValueFactory(generalPrac -> new ReadOnlyStringWrapper(generalPrac.getValue().getGeneralPractitioner()));

        updateProgramStatus();
    }


    /**
     * Private method for in class use only
     * Used when nodes are clicked to update
     * messages accordingly.
     */
    private void updateProgramStatus() {
        statusField.setText("Status: OK!");
        statusField.setTextFill(Color.WHITE);
        NewOrEditPatientController.selectedPatient = tableView.getSelectionModel().getSelectedItem();
    }







    @FXML
    private void exitWindow(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

    /**
     * Uses self defined Class ImportOrExportCSV
     * together with a fileChooser for easy access
     * to local file system.
     *
     * Method uses ExtensionFilter to make user select
     * a csv file.
     */

    @FXML
    public void exportToCSV() {
        if(App.register.getPatients().size() != 0) {
            fileChooser.setTitle("Save to .CSV");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));

            File destination = fileChooser.showSaveDialog(App.secondaryStage);
            if(App.register != null && destination != null) {
                middleMan.exportRegister(destination, App.register.getPatients());
            } else {
                statusField.setText("Problem exporting to file");
                statusField.setTextFill(Color.RED);
            }
        } else {
            statusField.setText("Patient list is empty");
            statusField.setTextFill(Color.RED);
        }

    }

    /**
     * Uses self defined Class ImportOrExportCSV
     * and fileChooser for easy access to local
     * files.
     *
     * Clears tableview and adds patient info
     * from csv file chosen locally.
     *
     * Method uses ExtensionFilter to make user select
     * a csv file.
     */

    @FXML
    public void importFromCSV()  {
        fileChooser.setTitle("Choose .CSV file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File toOpen = fileChooser.showOpenDialog(App.secondaryStage);

        if(toOpen != null) {

            App.register.getPatients().clear();
            try {
                for(Patient patient: middleMan.importRegister(toOpen)) {
                    App.register.addPatient(patient);
                }
            }catch (FileNotFoundException e) {
                statusField.setText("File selected was not found");
                statusField.setTextFill(Color.RED);
            }
            populateTableView();
        }
    }



    @FXML
    private void minimizeWindow(ActionEvent event) {
        ((Stage)((Button)event.getSource()).getScene().getWindow()).setIconified(true);
    }

    /**
     * Uses the sceneFactory to create and show an
     * instance of the about window.
     * @param event
     */

    @FXML
    public void openAboutWindow(ActionEvent event) {
        try {
            Stage s = SceneFactory.setNewStage("about");
            assert s != null;
            s.show();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Uses the sceneFactory to create and show an
     * instance of newOrEdit patient window.
     * Waits for window exit followed by
     * repopulating tableview in case of changes.
     *
     * @param event
     */
    @FXML
    public void openNewPatientWindow(ActionEvent event) {
        updateProgramStatus();
        adding = true;
        try{
            secondaryStage = SceneFactory.setNewStage("new");
            assert secondaryStage != null;
            secondaryStage.showAndWait();
        }catch (IOException e) {
            e.printStackTrace();
        }
        populateTableView();
    }


    /**
     * Same as method above, static variable 'editing' is
     * set to true to let 'NewOrEditPatientController' know
     * if user is adding or editing a new patient.
     *
     * Updates status
     *
     * @param event
     */

    @FXML
    public void openEditPatientWindow(ActionEvent event) {
        updateProgramStatus();
        if(NewOrEditPatientController.selectedPatient != null) {
            editing = true;
            try{
                secondaryStage = SceneFactory.setNewStage("edit");
                assert secondaryStage != null;
                secondaryStage.showAndWait();
            }catch (IOException e) {
                e.printStackTrace();
            }
            populateTableView();
        } else {
            statusField.setText("Status: No patient selected");
            statusField.setTextFill(Color.RED);
        }
    }


    /**
     * Method used with sceneFactory to instantiate
     * a confirmation window in case of miss clicks.
     */

    @FXML
    public void deletePatient() {
            updateProgramStatus();
            if(NewOrEditPatientController.selectedPatient != null) {
                try {
                    Stage c = SceneFactory.setNewStage("confirmation");
                    assert c != null;
                    c.showAndWait();
                }catch (IOException e) {
                    e.printStackTrace();
                }
                populateTableView();
            }
    }


    /**
     * Private method used for populating
     * the tableview in application
     *
     * updates status bar.
     */
    private void populateTableView() {
        tableView.getItems().clear();
        for (Patient patient : App.register.getPatients()) {
            tableView.getItems().add(patient);
        }
        tableView.sort();
        if(tableView.getItems().size() > 0) {
            updateProgramStatus();
        }
    }


}
