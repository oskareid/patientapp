package no.ntnu.idatt.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import no.ntnu.idatt.App;
import no.ntnu.idatt.Patient;

/**
 * Class to represent an instance of both
 * edit or add patient windows.
 */

public class NewOrEditPatientController {



    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label SSNLabel;

    @FXML
    private Label diagnosisLabel;

    @FXML
    private Label generalPractitionerLabel;

    @FXML
    private TextField newPatientFirstNameField;

    @FXML
    private TextField newPatientLastNameField;

    @FXML
    private TextField newPatientSSNField;

    @FXML
    private TextField diagnosisField;

    @FXML
    private TextField generalPractitionerField;

    @FXML
    private Button saveButton;

    public static Patient selectedPatient;

    /**
     * Collaborates with main controller to check if
     * user is adding or editing.
     */

    public void initialize() {
        if(MainController.editing) {
            saveButton.setText("Save");
            populateFields();
            newPatientSSNField.setDisable(true);
        } else if(MainController.adding) {
            saveButton.setText("Add");
            resetLabels();
        }
    }



    private void resetLabels() {
        newPatientSSNField.setDisable(false);
        newPatientSSNField.clear();
        newPatientFirstNameField.clear();
        newPatientLastNameField.clear();
        generalPractitionerField.clear();
        diagnosisField.clear();
    }

    private void populateFields() {
        newPatientFirstNameField.setText(selectedPatient.getFirstName());
        newPatientLastNameField.setText(selectedPatient.getLastName());
        newPatientSSNField.setText(selectedPatient.getSocialSecurityNumber());
        diagnosisField.setText(selectedPatient.getDiagnosis());
        generalPractitionerField.setText(selectedPatient.getGeneralPractitioner());
    }

    /**
     * Method for checking input-fields
     * and updating colors of labels
     * according to valid or invalid input
     * from user.
     *
     * @return true or false based on input.
     */

    private boolean checkInput() {
        boolean valid = true;
        String FN = newPatientFirstNameField.getText();
        String LN = newPatientLastNameField.getText();
        String DIA = diagnosisField.getText();
        String GP = generalPractitionerField.getText();

        if(FN.equals(" ") || FN.equals("")) {
            firstNameLabel.setTextFill(Color.RED);
            valid = false;
        } else {
            firstNameLabel.setTextFill(Color.BLACK);
        }

        if(LN.equals(" ") || LN.equals("")) {
            lastNameLabel.setTextFill(Color.RED);
            valid = false;
        } else {
            lastNameLabel.setTextFill(Color.BLACK);
        }

        if(MainController.adding) {
            if(!SSNCheck(newPatientSSNField.getText())) {
                SSNLabel.setTextFill(Color.RED);
                valid = false;
            } else {
                SSNLabel.setTextFill(Color.BLACK);
            }
        }

        if(DIA.equals(" ") || DIA.equals("")) {
            diagnosisLabel.setTextFill(Color.RED);
            valid = false;
        } else {
            diagnosisLabel.setTextFill(Color.BLACK);
        }

        if(GP.equals(" ") || GP.equals("")) {
            generalPractitionerLabel.setTextFill(Color.RED);
            valid = false;
        } else {
            generalPractitionerLabel.setTextFill(Color.BLACK);
        }
        return valid;
    }

    /**
     * Private method used in above method to
     * check more in-dept aspects of a valid SSN
     *
     * @param SSN social security number
     * @return true or false
     */

    private boolean SSNCheck(String SSN) {
        if(SSN.length() != 11) return false;
        for(int i = 0; i < SSN.length(); i++) {
            if(!Character.isDigit(SSN.charAt(i))) return false;
        }
        for(Patient patient: App.register.getPatients()) {
            if(patient.getSocialSecurityNumber().equals(SSN)) return false;
        }
        return true;
    }



    @FXML
    void closeWindow(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
        MainController.adding = false;
        MainController.editing = false;
        selectedPatient = null;
    }


    /**
     * Instantiates a Patient with input from text boxes.
     * Uses above methods to decide if user is
     * adding/editing and to change button text based on
     * those factors.
     * Adds/Edits selected patient if input passes the testing.
     *
     * @param event
     */
    @FXML
    void addOrEditPatient(ActionEvent event) {
        Patient patient = new Patient(newPatientFirstNameField.getText(), newPatientLastNameField.getText(),
                newPatientSSNField.getText(),diagnosisField.getText(), generalPractitionerField.getText());
        if(checkInput()) {
            if(saveButton.getText().equals("Save")) {
                App.register.deletePatient(patient);
            }
            if(App.register.addPatient(patient)) {
                closeWindow(event);
            }
        }
    }
}
