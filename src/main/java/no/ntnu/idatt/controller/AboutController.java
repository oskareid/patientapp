package no.ntnu.idatt.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Class represents a controller for instances of about Scene.
 */
public class AboutController {

    /**
     * Method for exiting about window.
     * @param event
     */
    @FXML
    void exitAboutWindow(ActionEvent event) {
        ((Stage)((Button)event.getSource()).getScene().getWindow()).close();
    }
}
