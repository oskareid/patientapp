package no.ntnu.idatt.sceneFactory;

import javafx.scene.Scene;

import java.io.IOException;

public interface NewScene {
    Scene constructScene() throws IOException;
}
