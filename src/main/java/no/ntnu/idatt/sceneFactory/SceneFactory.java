package no.ntnu.idatt.sceneFactory;

import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import no.ntnu.idatt.App;

import java.io.IOException;

/**
 * Class to represent a Scene Factory for convenience.
 *
 */

public class SceneFactory {
    /**
     * Static method for usage outside class.
     *
     * Method takes string as input information in order
     * to get desired scene from classes implementing interface 'NewScene'
     * Used to pass Stage to controller and display to user.
     *
     * @param sceneName
     * @return Stage to controller calling it
     * @throws IOException
     */

    public static Stage setNewStage(String sceneName) throws IOException {
        Stage stageToSet = new Stage();
        switch (sceneName) {
            case "about":
                AboutScene a = new AboutScene();
                stageToSet.setScene(a.constructScene());
                stageToSet.setTitle("About");
                break;
            case "confirmation":
                ConfirmScene c = new ConfirmScene();
                stageToSet.setScene(c.constructScene());
                stageToSet.setTitle("Delete confirmation");
                break;
            case "new":
                NewOrEditScene n = new NewOrEditScene();
                stageToSet.setScene(n.constructScene());
                stageToSet.setTitle("Add patient");
                break;
            case "edit":
                NewOrEditScene e = new NewOrEditScene();
                stageToSet.setScene(e.constructScene());
                stageToSet.setTitle("Edit patient");
                break;
            default:
                return null;
        }
        stageToSet.setAlwaysOnTop(true);
        stageToSet.setResizable(false);
        stageToSet.initOwner(App.primaryStage);
        stageToSet.initModality(Modality.WINDOW_MODAL);
        stageToSet.initStyle(StageStyle.UNDECORATED);
        return stageToSet;
    }



}
