package no.ntnu.idatt.sceneFactory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.Objects;

public class ConfirmScene implements NewScene {
    private Scene confirm;

    /**
     * Uses fxml file to construct a delete confirmation scene returned to
     * Class SceneFactory
     *
     * @return Scene
     * @throws IOException
     */

    @Override
    public Scene constructScene() throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass()
                .getResource("/fxml/confirmation.fxml")));

        confirm = new Scene(root);

        return confirm;
    }
}
