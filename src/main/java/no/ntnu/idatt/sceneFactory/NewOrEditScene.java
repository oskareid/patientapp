package no.ntnu.idatt.sceneFactory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.Objects;

/**
 * Uses fxml file to construct a new patient or
 * edit patient scene,
 * returned to Class SceneFactory
 */
public class NewOrEditScene implements NewScene {
    private Scene newOrEdit;


    @Override
    public Scene constructScene() throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/newOrEdit.fxml")));
        newOrEdit = new Scene(root);

        return newOrEdit;
    }
}
