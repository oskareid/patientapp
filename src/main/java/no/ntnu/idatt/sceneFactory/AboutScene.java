package no.ntnu.idatt.sceneFactory;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.Objects;

/**
 * Class to construct an About scene used in application
 */

public class AboutScene implements NewScene {
    private Scene about;

    /**
     * Uses fxml file to construct a about scene returned to
     * Class SceneFactory
     *
     * @return Scene
     * @throws IOException
     */

    @Override
    public Scene constructScene() throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/about.fxml")));
        about = new Scene(root);

        return about;
    }
}
