package no.ntnu.idatt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** Represents a patient register.
 *
 * @author Oskar Langås Eidem
 * @version 1.0
 * @since 1.0
 */

public class PatientRegister {

    private List<Patient> patients;

    public PatientRegister() {
        patients = new ArrayList<>();
    }

    /**
     * Two methods for the same functionality. One accepts an instance of Patient
     *
     * Methods takes use of private method 'doesPatientExist'
     * to decide if patient is unique to the system or not.
     * If patient is unique, patient is added to this patients list.
     *
     * @param firstName Patient's first name
     * @param lastName Patient's last name
     * @param socialSecNum Patient's social security number
     *
     */
    public boolean addPatient(String firstName, String lastName, String socialSecNum, String diagnosis, String generalPractitioner) {
        Patient patientToAdd = new Patient(firstName, lastName, socialSecNum, diagnosis, generalPractitioner);

        if(!(doesPatientExist(patientToAdd)) && SSNCheckForDigits(patientToAdd.getSocialSecurityNumber())) {
            patients.add(patientToAdd);
            return true;
        }
        return false;
    }

    public boolean addPatient(Patient patient) {
        if(!(doesPatientExist(patient)) && SSNCheckForDigits(patient.getSocialSecurityNumber())) {
            patients.add(patient);
            return true;
        }
        return false;
    }


    /**
     * Checks for technical SSN requirements like
     * length and if only digits.
     *
     * @param SSN social security number
     * @return true/false
     */
    private boolean SSNCheckForDigits(String SSN) {
        if(SSN.length() != 11) return false;
        for(int i = 0; i < SSN.length(); i++) {
            if(!Character.isDigit(SSN.charAt(i))) return false;
        }
        return true;
    }

    public void deletePatient(Patient patient) {
        patients.removeIf(patient1 -> patient1.equals(patient));
    }

    /**
     * private method for in this class use only.
     * Checks if Patient input is equal to patient(s) from this register.
     *
     * @param patientInput new Patient to check against existing patients.
     * @return boolean based on overwritten equals method in class 'Patient'
     */
    private boolean doesPatientExist(Patient patientInput) {
        Iterator<Patient> iterator = patients.iterator();
        Patient patientToCheck;
        while(iterator.hasNext()) {
            patientToCheck = iterator.next();
            if(patientToCheck.equals(patientInput)) return true;
        }
        return false;
    }


    /*------------------<getters_begin>--------------------*/



    public List<Patient> getPatients() {
        return patients;
    }
    /*------------------</getters_end>--------------------*/


    @Override
    public String toString() {
        return "PatientRegister{" +
                "patients=" + patients +
                '}';
    }


}
