package no.ntnu.idatt;

/** Represents a patient.
 *
 * @author Oskar Langås Eidem
 * @version 1.0
 * @since 1.0
 */

public class Patient {

    private final String SOCIAL_SECURITY_NUMBER;
    private String diagnosis;
    private String generalPractitioner;
    private String firstName;
    private String lastName;

    /**
     *
     * @param socialSecurityNumber represents any patient's unique social ID.
     * @param firstName patient first name
     * @param lastName patient last name
     * @param generalPractitioner Every patient has one
     * @param diagnosis physical/mental illness
     *
     * @Constuctor parameters above is required for instantiating Patient object.
     *             General practitioner and diagnose(s) can be set later.
     */

    public Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.SOCIAL_SECURITY_NUMBER = socialSecurityNumber;
            this.diagnosis = diagnosis;
            this.generalPractitioner = generalPractitioner;
    }



    /*------------------<getters_begin>--------------------*/

    public String getSocialSecurityNumber() {
        return SOCIAL_SECURITY_NUMBER;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }
    /*------------------</getters_end>--------------------*/




    /*------------------<setters_begin>--------------------*/

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setDiagnosis(String diagnosis) { this.diagnosis = diagnosis; }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }
    /*------------------</setters_end>--------------------*/


    /**
     *
     * @param obj Of type Object as method is overriding, not overloading.
     * @return boolean value based on equality between social security numbers.
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Patient)) return false;

        String objSSN = ((Patient)obj).getSocialSecurityNumber();
        return this.getSocialSecurityNumber().equals(objSSN);
    }

    @Override
    public int hashCode() {
        int result = SOCIAL_SECURITY_NUMBER.hashCode();
        result = 31 * result;
        return result;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + SOCIAL_SECURITY_NUMBER + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis=" + diagnosis +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
