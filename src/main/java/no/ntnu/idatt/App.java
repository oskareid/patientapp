package no.ntnu.idatt;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class App extends Application {
    private static Scene mainScene;
    public static Stage secondaryStage;
    public static Stage primaryStage;
    public static PatientRegister register = new PatientRegister();


    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Patient Register");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setMinWidth(664);
        stage.setMinHeight(555);
        mainScene = new Scene(loadFXML("main"));
        stage.setScene(mainScene);
        primaryStage = stage;
        primaryStage.show();
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
}
